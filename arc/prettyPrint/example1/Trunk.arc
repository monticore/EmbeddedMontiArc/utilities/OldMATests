/* (c) https://github.com/MontiCore/monticore */

package example1;
import example1.datatypes.Types;

component Trunk {
  port  in ChangeCmd,
        out DoorStat;
}
