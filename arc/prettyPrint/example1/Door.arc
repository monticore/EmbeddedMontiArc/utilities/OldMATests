/* (c) https://github.com/MontiCore/monticore */

package example1;

import example1.datatypes.Types.*;

component Door {

    port in ChangeCmd cmd,
         out DoorStat ok;
         
}
