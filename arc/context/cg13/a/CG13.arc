/* (c) https://github.com/MontiCore/monticore */

package a;
import java.lang.String;

component CG13 {
  component CG13false1<String> f1;
  component CG13false2("5") f2;
  
  component CG13true1<String> t1;
  component CG13true2(5) t2;
}
