/* (c) https://github.com/MontiCore/monticore */

component Init {
  
  
  component Ref someName;
  
  component Ref2 anotherName;
}
