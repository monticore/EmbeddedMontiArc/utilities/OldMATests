/* (c) https://github.com/MontiCore/monticore */

package y;

component R6GenericPartner<T> {
  
  port
    in T tIn,
    out T tOut;
}
