/* (c) https://github.com/MontiCore/monticore */

package x;
import myTypes.DefinedType;

component R2RefQualifiedImported {
  port in DefinedType;
}
