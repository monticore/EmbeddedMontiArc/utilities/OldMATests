/* (c) https://github.com/MontiCore/monticore */

package x;

component CG2Partner<T> {
  
  port 
    in T tIn1,
    in T tIn2,
    in T tIn3,
    out T tOut;

}
