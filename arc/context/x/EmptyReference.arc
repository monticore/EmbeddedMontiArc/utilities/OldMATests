/* (c) https://github.com/MontiCore/monticore */

package x;

component EmptyReference {

  port
    in String pIn,
    out String pOut;

}
