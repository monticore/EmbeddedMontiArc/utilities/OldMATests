/* (c) https://github.com/MontiCore/monticore */

package x;
import java.lang.Boolean;

component R5Partner {

  port 
    in String portIn,
    in Boolean,
    out Integer portOut;

}
