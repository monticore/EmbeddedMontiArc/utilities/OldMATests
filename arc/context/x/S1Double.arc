/* (c) https://github.com/MontiCore/monticore */

package x;
import java.lang.String;
import java.io.Serializable;

component S1Double {
  port 
    in String,
    in Serializable;
}
