/* (c) https://github.com/MontiCore/monticore */

package a;

component CG5<T> {
  
  port 
    in T,
    out T named,
    out String;

}
