/* (c) https://github.com/MontiCore/monticore */

package a;

import z.T;
import z.K;

component CG6<K, T> {
  
  port 
    in T someName, 
    in z.T correct,
    out z.K correctOut,
    out K anotherK;

}
