/* (c) https://github.com/MontiCore/monticore */

package a;

component CG3false {
  
  port 
    in T somePort,
    out V anotherPort;

} 
