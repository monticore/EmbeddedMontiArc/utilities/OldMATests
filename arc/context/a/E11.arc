/* (c) https://github.com/MontiCore/monticore */

package a;

import java.lang.Integer;

component E11 {
 port 
   in Integer,
   out Integer someIntName;
}
