/* (c) https://github.com/MontiCore/monticore */

package a;

component E2 {

  component x.EmptyReference;
  component x.EmptyReference;
  
  component x.R2RefQualifiedImported someName;
  component y.R2RefPackageImported someName;
  
  component x.R6PartnerOne r6PartnerOne;
  component x.R6PartnerOne;
  
  component y.R6GenericPartner<java.lang.String>;
  
}
