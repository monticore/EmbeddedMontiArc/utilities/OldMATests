/* (c) https://github.com/MontiCore/monticore */

package a;

import x.R2RefQualifiedImported;
import y.*;


component R2 {
  
  component R2RefSamePackage;
  component R2RefQualifiedImported;
  component R2RefPackageImported;
  component z.R2RefFQ;
  component x.UndefinedReferenceFQ;
  component UndefinedReference;
}
