/* (c) https://github.com/MontiCore/monticore */

package a;

component CG3true<T> {
  
  port 
    in T somePort,
    out T anotherPort;

} 
