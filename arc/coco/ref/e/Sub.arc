/* (c) https://github.com/MontiCore/monticore */

package e;

component Sub extends Super {
    port 
        in String;
}
