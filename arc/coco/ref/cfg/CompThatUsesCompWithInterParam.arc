/* (c) https://github.com/MontiCore/monticore */

package cfg;

component CompThatUsesCompWithInterParam {
    
    component CompWithInterParam<String>(new Impl());
}
