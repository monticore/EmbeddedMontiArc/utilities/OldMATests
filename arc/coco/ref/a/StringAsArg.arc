/* (c) https://github.com/MontiCore/monticore */

package a;

component StringAsArg(String s) {

    port
        in String sIn;
}
