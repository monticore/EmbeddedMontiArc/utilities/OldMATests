/* (c) https://github.com/MontiCore/monticore */

package a;

import f.MyEnum;

component EnumAsTypeArg(MyEnum varName) {
    port 
        in String sIn;

}
