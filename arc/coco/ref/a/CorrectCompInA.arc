/* (c) https://github.com/MontiCore/monticore */

package a;

component CorrectCompInA {

    port
        in String stringIn,
        out String stringOut;

}
