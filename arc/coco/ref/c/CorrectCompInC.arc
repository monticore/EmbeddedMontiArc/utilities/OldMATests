/* (c) https://github.com/MontiCore/monticore */

package c;

component CorrectCompInC {

    port
        in String stringIn,
        out String stringOut;

}
