/* (c) https://github.com/MontiCore/monticore */

package b;

component SimpleGenericComponent<K, V> {
    
    port 
        in String stringIn;
}
