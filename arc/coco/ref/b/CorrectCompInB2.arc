/* (c) https://github.com/MontiCore/monticore */

package b;

component CorrectCompInB2 {

    port
        in String stringIn,
        out String stringOut;

}
