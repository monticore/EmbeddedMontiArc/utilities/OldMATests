/* (c) https://github.com/MontiCore/monticore */

package b;

component CorrectCompInB {

    port
        in String stringIn,
        out String stringOut;

}
