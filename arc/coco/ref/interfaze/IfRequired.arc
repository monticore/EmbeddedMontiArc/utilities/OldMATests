/* (c) https://github.com/MontiCore/monticore */

package interfaze;

component IfRequired {
    port
        in MyImpl implIn,
        in MyInterface ifIn;

}
