/* (c) https://github.com/MontiCore/monticore */

package interfaze;

component IfProvider {
    
    port
        out MyImpl implOut,
        out MyInterface ifOut;

}
