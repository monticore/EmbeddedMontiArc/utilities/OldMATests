/* (c) https://github.com/MontiCore/monticore */

package a;

component SuperComponentWithPort {
    
    port 
        in String strIn;

}
