/* (c) https://github.com/MontiCore/monticore */
package ma.sim;

component ConstantDelay<T>(int delay) {
  
  timing delayed;
  
  port 
    in T portIn,
    out T portOut;
   
}
