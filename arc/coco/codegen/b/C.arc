/* (c) https://github.com/MontiCore/monticore */

package b;

component C {
    port
        in String sIn1,
        in String sIn2,
        out String sOut;

}
