/* (c) https://github.com/MontiCore/monticore */

package b;

component A {
    port
        in String sIn,
        out String sOut;
}
