/* (c) https://github.com/MontiCore/monticore */

package b;

component B {
    
    timing delayed;
    
    port
        in String sIn,
        out String sOut;
}
