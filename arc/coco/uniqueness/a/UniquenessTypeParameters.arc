/* (c) https://github.com/MontiCore/monticore */

package a;

component UniquenessTypeParameters<K, K, V, T, T> {
    port 
        in String s1;
}
