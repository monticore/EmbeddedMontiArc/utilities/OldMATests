/* (c) https://github.com/MontiCore/monticore */

package a;

component E1_2 {
    port
        in Object,
        in Throwable,
        out Throwable throwable,
        in Integer,
        out Integer someIntName;
}
