/* (c) https://github.com/MontiCore/monticore */

package a;


component E1 {
  port
    in String,
    out String,
    in Boolean named,
    out Boolean named;
}
