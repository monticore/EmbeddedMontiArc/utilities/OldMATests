/* (c) https://github.com/MontiCore/monticore */

package a;

component UniqueInnerComponentNames(int foo) {
    
    port
      in String foo;
    
    component Inner foo {
    
    }
    
}
