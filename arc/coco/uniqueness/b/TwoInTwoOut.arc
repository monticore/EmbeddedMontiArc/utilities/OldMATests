/* (c) https://github.com/MontiCore/monticore */

package b;

component TwoInTwoOut {
    
    port
        in String stringIn,
        in Integer intIn,
        out String stringOut,
        out Integer intOut;
}
