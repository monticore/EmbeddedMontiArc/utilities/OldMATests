/* (c) https://github.com/MontiCore/monticore */

package conv;

component SimpleComp {
    
    port
        in Integer usedInputInteger,
        out Integer usedOutputInteger,
        out Integer unusedSimpleCompOutputInteger;
}
