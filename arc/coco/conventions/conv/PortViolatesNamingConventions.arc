/* (c) https://github.com/MontiCore/monticore */

package conv;

component PortViolatesNamingConventions {
    
    port
        in String Violation,
        out Boolean nonViolation;
}
