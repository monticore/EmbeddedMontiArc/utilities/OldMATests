/* (c) https://github.com/MontiCore/monticore */

package conv;

component CorrectComp {

    port
        in String stringIn,
        out String stringOut;

}
