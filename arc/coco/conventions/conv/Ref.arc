/* (c) https://github.com/MontiCore/monticore */

package conv;

component Ref {
    port 
        in String s1;
}
