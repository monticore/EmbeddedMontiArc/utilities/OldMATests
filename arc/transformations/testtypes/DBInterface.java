/* (c) https://github.com/MontiCore/monticore */

package testtypes;

public interface DBInterface {

  public void doFoo();

  public java.lang.String getBar();
}
