/* (c) https://github.com/MontiCore/monticore */

package d;

component D {
    port 
        in String dataSthElse,
        in Integer myInt,
        in Boolean bool,
        out String strOut,
        out String sthElse,
        out Integer intOut;
        
}
