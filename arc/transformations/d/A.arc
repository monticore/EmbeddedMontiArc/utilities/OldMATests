/* (c) https://github.com/MontiCore/monticore */

package d;

component A {
    port
        in String strIn,
        out String data;

}
