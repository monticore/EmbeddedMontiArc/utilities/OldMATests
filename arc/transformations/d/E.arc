/* (c) https://github.com/MontiCore/monticore */

package d;

component E {
    port
        in String sIn,
        out String sOut;

}
