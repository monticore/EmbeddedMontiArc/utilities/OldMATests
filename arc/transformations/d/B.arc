/* (c) https://github.com/MontiCore/monticore */

package d;

component B {
    
    port
        in String strIn,
        in Integer intIn,
        out Integer myInt;
}
