/* (c) https://github.com/MontiCore/monticore */

package d;

component C {
    port 
        in Integer intIn,
        out Boolean bb;
}
