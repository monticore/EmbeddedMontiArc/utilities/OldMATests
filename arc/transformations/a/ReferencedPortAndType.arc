/* (c) https://github.com/MontiCore/monticore */

package a;

component ReferencedPortAndType {

    port
        in Integer a,
        in Integer b,
        in Integer c,
        out String x;
}
