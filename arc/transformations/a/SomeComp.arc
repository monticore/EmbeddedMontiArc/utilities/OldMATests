/* (c) https://github.com/MontiCore/monticore */

package a;
import java.util.List;

component SomeComp {
    port
        in String myIn,
        in List myListIn,
        out String myOut;
    
}
