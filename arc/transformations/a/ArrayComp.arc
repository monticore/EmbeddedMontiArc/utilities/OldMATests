/* (c) https://github.com/MontiCore/monticore */

package a;

component ArrayComp {
    port
        in String[] strIn1,
        in String[][] strIn2,
        out String[] strOut1,
        out String[][] strOut2;
    
}
