/* (c) https://github.com/MontiCore/monticore */

package a;

component AutoConnectGenericUsageHierarchie {
    autoinstantiate on;
    autoconnect type;
    
    port 
        in String strIn,
        out Object objOut;
    
    
    component Inner<T> sInner<Object> {
        port
            in T tIn,
            out T tOut;
    }
 
}
