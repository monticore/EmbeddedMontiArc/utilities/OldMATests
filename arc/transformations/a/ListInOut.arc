/* (c) https://github.com/MontiCore/monticore */

package a;
import java.util.List;

component ListInOut {
    
    port
        in List lIn,
        out List lOut;
}
