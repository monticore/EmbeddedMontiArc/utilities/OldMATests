/* (c) https://github.com/MontiCore/monticore */

package a;

import java.util.*;

component Ports {
    
    port 
        in String p1,
        out List p2;

}
