/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface IComplex1 extends sim.generic.IComponent,
    simTypes.gen.ports.Complex1PortInterface {

}
