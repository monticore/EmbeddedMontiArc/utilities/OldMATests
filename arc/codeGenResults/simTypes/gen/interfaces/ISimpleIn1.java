/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface ISimpleIn1 extends sim.generic.IComponent,
    simTypes.gen.ports.SimpleIn1PortInterface {
}
