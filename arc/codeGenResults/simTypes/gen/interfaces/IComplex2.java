/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface IComplex2 extends sim.generic.IComponent,
    simTypes.gen.ports.Complex2PortInterface {
}
