/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface ISimpleOut2 extends sim.generic.IComponent,
    simTypes.gen.ports.SimpleOut2PortInterface,
    sim.generic.IOutgoingPort<java.lang.Integer> {
}
