/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface ISimpleIn2 extends sim.generic.IComponent,
    simTypes.gen.ports.SimpleIn2PortInterface,
    sim.generic.IIncomingPort<java.lang.String> {
}
