/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface IComplex3 extends sim.generic.IComponent,
    simTypes.gen.ports.Complex3PortInterface {
}
