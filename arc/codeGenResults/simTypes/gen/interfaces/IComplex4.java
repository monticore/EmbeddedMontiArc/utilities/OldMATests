/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface IComplex4 extends sim.generic.IComponent,
    simTypes.gen.ports.Complex4PortInterface {
}
