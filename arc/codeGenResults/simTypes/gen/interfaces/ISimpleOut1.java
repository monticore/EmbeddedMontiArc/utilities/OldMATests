/* (c) https://github.com/MontiCore/monticore */

package simTypes.gen.interfaces;

public interface ISimpleOut1 extends sim.generic.IComponent,
    simTypes.gen.ports.SimpleOut1PortInterface {
}
