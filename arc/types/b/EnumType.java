/* (c) https://github.com/MontiCore/monticore */

package b;

public enum EnumType {
  A,
  B,
  C;
}
