/* (c) https://github.com/MontiCore/monticore */

package factoryTest;

component Basic {
  
  port 
    in String,
    out Boolean bool;

}
