/* (c) https://github.com/MontiCore/monticore */

package componentTest;
import java.lang.Integer;
import java.lang.String;

component SimpleIn {
  
  port
    in String,
    out Integer int1,
    out Integer int2;
}
