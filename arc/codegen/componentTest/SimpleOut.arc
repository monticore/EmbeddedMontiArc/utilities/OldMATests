/* (c) https://github.com/MontiCore/monticore */

package componentTest;
import java.lang.Integer;
import java.lang.String;

component SimpleOut {
  
  port
    in String str1,
    in String str2,
    out Integer;
}
