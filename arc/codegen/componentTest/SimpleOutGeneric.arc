/* (c) https://github.com/MontiCore/monticore */

package componentTest;

import java.lang.String;

<<generic>> component SimpleOutGeneric {
  port
    in T input1,
    in T input2,
    in String,
    out T output;

}
