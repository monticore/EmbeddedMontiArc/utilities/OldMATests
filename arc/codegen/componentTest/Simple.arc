/* (c) https://github.com/MontiCore/monticore */

package componentTest;
import java.lang.Integer;
import java.lang.String;

component Simple {
  
  port
    in String,
    out Integer;
}
