/* (c) https://github.com/MontiCore/monticore */

package portTest;

component Basic2 {
  
  port 
    in String,
    in Integer,
    in Boolean bool;

}
