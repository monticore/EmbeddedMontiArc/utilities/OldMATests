/* (c) https://github.com/MontiCore/monticore */

package portTest;

component Basic {
  
  port 
    in String,
    out Boolean bool;

}
