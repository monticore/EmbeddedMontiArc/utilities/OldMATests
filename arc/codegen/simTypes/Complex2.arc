/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component Complex2 {
  
  port
    in String str1,
    in String str2;
}
