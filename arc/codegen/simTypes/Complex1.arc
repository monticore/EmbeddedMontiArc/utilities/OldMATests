/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component Complex1 {
  
  port
    in String str1,
    in String str2,
    out Integer int1,
    out Integer int2;
}
