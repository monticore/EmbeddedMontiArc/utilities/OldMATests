/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component SimpleOut2 {
  
  port
    out Integer;
}
