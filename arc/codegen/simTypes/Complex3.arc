/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component Complex3 {
  
  port
    out Integer int1,
    out Integer int2;
}
