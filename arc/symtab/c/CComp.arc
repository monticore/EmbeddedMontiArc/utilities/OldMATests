/* (c) https://github.com/MontiCore/monticore */

package c;

component CComp {
  ports in Integer in1,
        out Boolean out1;

}
