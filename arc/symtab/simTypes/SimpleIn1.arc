/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component SimpleIn1 {
  
  port
    in String,
    out Integer int1,
    out Integer int2;
}
