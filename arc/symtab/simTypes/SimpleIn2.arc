/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component SimpleIn2 {
  
  port
    in String;
}
