/* (c) https://github.com/MontiCore/monticore */

package simTypes;

component Simple {
  
  port
    in String,
    out Integer;
}
