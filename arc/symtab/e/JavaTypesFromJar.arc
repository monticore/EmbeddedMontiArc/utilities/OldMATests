/* (c) https://github.com/MontiCore/monticore */

package e;

import java.util.List;

component JavaTypesFromJar {
    
    port 
        in List l;    

}
