/* (c) https://github.com/MontiCore/monticore */

package f;

component CompUsingCompInDefaultPackage {
    
    component CompInDefaultPackage;
}
