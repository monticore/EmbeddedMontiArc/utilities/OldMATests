/* (c) https://github.com/MontiCore/monticore */

package params;

import java.util.*;

component UsingComplexGenericParams<K, V> {
    autoinstantiate on;
    component ComplexGenericParams<K, V> (new int[]{1, 2, 3}, new HashMap<List<K>, List<V>>()) cp;
     
}
