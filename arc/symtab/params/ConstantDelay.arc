/* (c) https://github.com/MontiCore/monticore */
package params;

component ConstantDelay<T>(int delay) {
  
  timing instant;
  
  port 
    in T portIn,
    out T portOut;
   
}
