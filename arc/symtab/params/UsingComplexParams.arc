/* (c) https://github.com/MontiCore/monticore */

package params;

import java.util.*;

component UsingComplexParams {
    autoinstantiate on;
    component ComplexParams(new int[]{1, 2, 3}, new HashMap<List<String>, List<Integer>>()) cp;
     
}
