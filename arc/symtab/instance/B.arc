/* (c) https://github.com/MontiCore/monticore */

package instance;

component B {
  ports in Integer portB1,
        in Integer portB2,
        in Integer portB3;
}
