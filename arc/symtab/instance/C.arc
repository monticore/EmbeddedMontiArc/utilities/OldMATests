/* (c) https://github.com/MontiCore/monticore */

package instance;

component C extends A {
  ports in Integer portC1,
        in Integer portC2,
        in Integer portC3,
        out Boolean portC4;
}
