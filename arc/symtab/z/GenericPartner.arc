/* (c) https://github.com/MontiCore/monticore */

package z;

component GenericPartner<T> {
  
  port
    in T tIn,
    out T tOut;
}
