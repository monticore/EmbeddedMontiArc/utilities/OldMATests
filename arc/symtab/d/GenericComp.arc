/* (c) https://github.com/MontiCore/monticore */

package d;

component GenericComp<I, O> {
  port 
    in I portIn;
}
