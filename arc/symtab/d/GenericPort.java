/* (c) https://github.com/MontiCore/monticore */

package d;

import java.util.List;

public class GenericPort<K, V> {
  public static final List<K> genericFieldOne;
  public static final V genericFieldTwo;
}
