/* (c) https://github.com/MontiCore/monticore */

package a;

component UsingCompWithCfgArgs {

    component CompWithCfgArgs(new Person());
}
