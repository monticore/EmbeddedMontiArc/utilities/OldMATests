/* (c) https://github.com/MontiCore/monticore */

package a.myTypes;

public class NewType<K, V> {
  public K key;
  public V value;
}
