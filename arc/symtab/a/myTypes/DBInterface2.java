/* (c) https://github.com/MontiCore/monticore */

package a.myTypes;

public interface DBInterface2<K, V> {

  public K doFoo2();

  public V getBar2();
}
