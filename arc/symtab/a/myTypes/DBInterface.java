/* (c) https://github.com/MontiCore/monticore */

package a.myTypes;

public interface DBInterface {

  public void doFoo();

  public java.lang.String getBar();
}
