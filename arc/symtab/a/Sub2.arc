/* (c) https://github.com/MontiCore/monticore */

package a;

component Sub2 extends b.SuperSamePackage {
  
  port  out String, 
        out Integer;
}
