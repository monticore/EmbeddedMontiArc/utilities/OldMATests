/* (c) https://github.com/MontiCore/monticore */

package ocl;

component OCLFieldToArcField(int foo) {
  
    
  ocl inv myInv:
    foo <= 5;
  
  
}
