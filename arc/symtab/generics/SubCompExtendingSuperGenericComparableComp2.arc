/* (c) https://github.com/MontiCore/monticore */

package generics;

component SubCompExtendingSuperGenericComparableComp2<V extends Comparable<V>, T> extends SuperGenericComparableComp2<T, V> {

}
