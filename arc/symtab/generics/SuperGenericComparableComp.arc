/* (c) https://github.com/MontiCore/monticore */

package generics;

component SuperGenericComparableComp<T extends Comparable<T>> {

    port
        in T tIn,
        out T tOut;
}
