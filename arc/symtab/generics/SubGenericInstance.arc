/* (c) https://github.com/MontiCore/monticore */

package generics;

component SubGenericInstance {
  component SubGeneric<Integer> sg;
}
