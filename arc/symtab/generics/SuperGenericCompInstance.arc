/* (c) https://github.com/MontiCore/monticore */

package generics;

component SuperGenericCompInstance {
  component SuperGenericComparableComp2<Integer, Double> sgc;
  component SuperGenericComparableComp2<String, Boolean> sgc2;
}
