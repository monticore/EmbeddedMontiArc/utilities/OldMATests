/* (c) https://github.com/MontiCore/monticore */

package generics;

component BaseClassGenerics extends SubGeneric<String> {
  ports in Boolean boolIn,
        out Integer intOut;
}
