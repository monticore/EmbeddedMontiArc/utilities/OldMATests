/* (c) https://github.com/MontiCore/monticore */

package generics;

component SubCompExtendsGenericComparableCompValid<K extends Comparable<K>> extends SuperGenericComparableComp<K> {

}
