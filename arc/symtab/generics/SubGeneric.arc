/* (c) https://github.com/MontiCore/monticore */

package generics;

component SubGeneric<T> {
    
    port
        in T sIn1,
        in T sIn2,
        out T sOut;

}
