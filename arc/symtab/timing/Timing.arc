/* (c) https://github.com/MontiCore/monticore */

package timing;

component Timing {
    autoinstantiate on;

    component TimedInner {
        timing instant;
    }
    
    component TimedDelayingInner {
        timing delayed;
    }
    
    component TimeSyncInner {
        timing sync;
    }
    
    component TimeCausalSyncInner {
        timing causalsync;
    }
    
    component UntimedInner {
        timing untimed;
    }

}
