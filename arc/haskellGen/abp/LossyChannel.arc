/* (c) https://github.com/MontiCore/monticore */

package abp;

component LossyChannel {
  port 
    in ABPMessage<T> portIn,
    out T portOut;
}
