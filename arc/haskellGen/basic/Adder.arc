/* (c) https://github.com/MontiCore/monticore */

package basic;

component Adder {  
  port
    in Integer num1,
    in Integer num2,
    out Integer sum;
}
