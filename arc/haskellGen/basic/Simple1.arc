/* (c) https://github.com/MontiCore/monticore */

package basic;

component Simple1 {
  
  port
    in Integer in1,
    in Integer in2,
    out Integer outp;
}
